num add(num x, num y) {
    return x + y;
}

Map<String, num> switchNumber(num num1, num2) {
    return {
        'num1' : num2,
        'num2' : num1
    };
    
}

int? countLetter(String letter, String sentence) {
    int result = 0;


    if (letter.length > 1){
            return null;
    }else {
        for (var i= 0 ; i<sentence.length; i++){
            if (sentence[i] == letter){
                result = result + 1;
            }
        }
        return result;
    }
}

bool isPalindrome(String text) {
    
    String finalText = text.split(' ').join('');
    finalText = finalText.toLowerCase();
    String reverse = '';

    for(int i = 0; i<finalText.length; i++){
        reverse = reverse + finalText[(finalText.length - 1 ) -i];
    }
    if(finalText == reverse){
        return true;
    } else {
        return false;
    }
}

bool isIsogram(String text) {

    var normalized = (text.toLowerCase().split(""));
    return normalized.toSet().length == normalized.length;
   
}

double? purchase(int age, num price) {
    var discountedPrice = price * 0.8;
    
    if (age < 13 ){
        return (null);
    } else if ( 13 <= age && age <= 21 ){
        return double.parse(discountedPrice.toStringAsFixed(2));
    } else if (60 <= age ){
        return double.parse(discountedPrice.toStringAsFixed(2));
    } else if (age > 22 && age < 64){
        return double.parse(price.toStringAsFixed(2));
    };
}


List<String> findHotCategories(List items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    List<String> newItem = [];

    for (var i = 0 ; i<items.length ; i++){
        if(items[i]['stocks'] == 0){
            if(newItem.contains(items[i]['category'])){
                newItem;
            } else {
                newItem.add(items[i]['category']);
            }    
        }                   
    }
    return newItem;
}

List<String> findFlyingVoters(List candidateA, List candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    List<String> newArr =[];
   

    for (int j = 0; j < candidateA.length ; j++ ){  
        for (int i = 0; i < candidateB.length ; i++ ){           
            if (candidateA[j] == candidateB[i]){
                newArr.add(candidateA[j]);     
            }   
        }   
    }
    return newArr;
}

void main () {
    calculateAdEfficiency([{ 'brand': 'Brand X', 'expenditure': 12345.89, 'customersGained': 4879 },
            { 'brand': 'Brand Y', 'expenditure': 22456.17, 'customersGained': 6752 },
            { 'brand': 'Brand Z', 'expenditure': 18745.36, 'customersGained': 5823 }]);
}


List<Map<String, dynamic>> calculateAdEfficiency(List items) {

    var adEfficiency;
    var brand ;
    List<Map<String, dynamic>> arr =[];
    

    for (int i = 0; i < items.length; i++){
        adEfficiency = items[i]['customersGained']/items[i]['expenditure'] *100;
        brand = items[i]['brand'];
        arr.add({'brand': brand , 'adEfficiency': adEfficiency});
    }

    int compare (a,b){
        var adEfficiencyA = a['adEfficiency'];
        var adEfficiencyB = b['adEfficiency'];

        int comparison = 0; 
        if (adEfficiencyA > adEfficiencyB){
            comparison = -1 ;
        }else if (adEfficiencyA < adEfficiencyB){
            comparison = 1 ;
        }
        return comparison;
    }
    arr.sort(compare);
    print (arr) ;
    return arr;
    
}